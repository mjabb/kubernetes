[1]: https://www.tecmint.com/install-a-kubernetes-cluster-on-centos-8/
[2]: https://www.howtoforge.com/tutorial/centos-kubernetes-docker-cluster/
[3]: https://download.docker.com/linux/centos/8/x86_64/stable/Packages/
[4]: https://github.com/flannel-io/flannel
[5]: https://www.weave.works/docs/net/latest/overview/
[6]: https://stackoverflow.com/questions/47845739/configuring-flannel-to-use-a-non-default-interface-in-kubernetes
[7]: http://linuxsoft.cern.ch/centos/8-stream/isos/x86_64/
[8]: https://www.centos.org/
[9]: https://github.com/kubernetes/dashboard/blob/master/docs/user/installation.md
[10]: https://www.rosehosting.com/blog/how-to-generate-a-self-signed-ssl-certificate-on-linux/
[11]: https://www.tecmint.com/install-nfs-server-on-centos-8
[15]: https://www.jenkins.io/doc/book/installing/kubernetes/
[51]: https://kubernetes.io/docs/concepts/storage/volumes/
[52]: https://kubernetes.io/docs/concepts/storage/persistent-volumes/
[70]: https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data/
[90]: https://kubernetes.io/docs/tasks/run-application/run-single-instance-stateful-application/

# CentOS Installation und Konfiguration

## Lab Umgebung

Die Umgebung basiert auf 3 virtuellen [CentOS 8][8] Server sowie einem optionalen [CentOS][8] NFS Server.

Für die Installtion wird das boot.iso verwendet unter folgendem Link verwendet:

https://linuxsoft.cern.ch/centos/8-stream/isos/x86_64/



Die Anleitung basiert auf einer Virtual-Box Virtuallisierung mit einem Host-Only Network. Prinzipiell kann aber irgend eine Virtuallisierung verwendet werden. 

### VirtualBox VM Setup 

* 3 VM mit **2 CPU* / 2Gb RAM / 32Gb Disk
* 2 Network (NAT / Host-Only - 192.168.11.0)
* Headless [CentOS 8][8] Server Installation


## Installation auf Master- und Worker-Node

Nachfolgende Installationen finden sowohl auf dem **Master-Node** und den **Worker-Nodes** statt. Daher wird lediglich der Master installiert und anschliessend mit neuen **UUID** und **MAC-Adressen** zu Workernodes geklont. Die einzelnen Schritte  folgen dazu später.

### CentOS Installation

||Setting|Bemerkung|
|--|--|--|
|OS|[CentOS 8][8]| Version 8 Stream|
|Sprache|English (United States)|Systemsprache|
|Keyboard | Swiss German (German (Switzerland))|muss Hostsystem entsprechen| 
|Software Selection|Minimal Install| +Standard +Headless Management| 
|Installation Source|closest mirror|Alternativ CDROM verwenden|
|Installation Destination| Disk0 (sda)|*Atomatic partitioning* verwenden|
|Hostname|master.k8s<br>worker1.k8s<br>worker2.k8s|Hostname Worker-Nodes nach Klonen mit `set-hostnamectl` setzen|
|Network enp0s3| DHCP | SSH Port 22 Forward für VM Zugriff einrichten |
|Network enp0s8| 192.168.11.11/24|Hostname Worker-Nodes nach Klonen mit `hostnamectl set-hostname <hostname>` setzen|
|Date & Time | Europe/Zurich |  |

#### Hosts eintragen

Hosts in `/etc/hosts` eintragen. Folgende IP Adressen und Namen werden verwendet:

| name |domain | IP  |
|:--:|:--:|:--:|
| master |k8s| 192.168.11.11 |
| worker1 |k8s| 192.168.11.12 |
| worker2|k82s| 192.168.11.13 |

```
# cat <<EOF>> /etc/hosts
192.168.11.11 master.k8s master
192.168.11.12 worker1.k8s worker1
192.168.11.13 worker2.k8s worker2
EOF
```

#### Disable SELinux

```
# setenforce 0
# sed -i --follow-symlinks 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux
```

#### disable SWAP

Runtime ` swapoff -a ` und permanent in `/etc/fstab`   mit `#` kommentieren.

```
# /dev/mapper/cs_master--node-swap none                    swap    defaults        0 0
```

#### Firewall stoppen und deaktivieren

```
# systemctl stop firewalld
# systemctl disable firewalld
```

####  br_netfilter kernel module aktivieren

```
# modprobe br_netfilter
# echo '1' > /proc/sys/net/bridge/bridge-nf-call-iptables
```

#### Dependency Pakete für docker-ce installieren

```
# dnf install -y yum-utils device-mapper-persistent-data lvm2
```

## Docker

#### Repository zufügen

```
# dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
```

Zudem muss **containerd.io** installiert werden. Die aktuellen Versionen für [CentOS 8][8] kann [hier][3] downloaded werden.

Da *Podman* bereits installiert ist, muss die Option `--allowerasing` verwendet werden um *Podman* zu deinstallieren.

```
# dnf install -y --allowerasing https://download.docker.com/linux/centos/8/x86_64/stable/Packages/containerd.io-1.4.4-3.1.el8.x86_64.rpm
```

#### Docker installieren und starten

```
# dnf install -y docker-ce
# systemctl start docker
```

#### Cgroup Driver festlegen

**Systemd** als **Cgroup Driver** gemäss dieser [Anweisung](https://stackoverflow.com/questions/43794169/docker-change-cgroup-driver-to-systemd) für Docker definieren. Dazu das File `/etc/docker/daemon.json` editieren oder erstellen.

`# vi /etc/docker/daemon.json`

und folgenden Code zufügen

```
{
  "exec-opts": ["native.cgroupdriver=systemd"]
}
```

anschliessend Docker restarten.

```
# systemctl restart docker
```


Testen das `systemd` nun der standard *Cgroup Driver* ist

```
# docker info |grep Cgroup
 Cgroup Driver: systemd
 Cgroup Version: 1
```


## Kubernetes 

#### Repository zufügen

```
# cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
```

#### Kubernetes installieren

Kubernetes und benötigte Packete installieren

 ```
 # dnf install -y kubeadm kubelet kubectl iproute-tc-5.9.0-4.el8.x86_64
 ```

 Kubernetes ist jetzt installiert, nun ist es Zeit für einen `reboot`

 ```
 # sync
 # reboot
 ```




#### Docker und Kubernetes starten

```
# systemctl start docker && systemctl enable docker
# systemctl start kubelet && systemctl enable kubelet
```

> Die **Basisinstallation** ist hiermit beendet. Nun muss die VM mit einem `poweroff` gestoppt werden um davon **Klone** für die **Worker-Nodes** zu erstellen.



